const jwt = require('jsonwebtoken')
require('dotenv').config()
function generateAdminToken(User){
    const token = jwt.sign(User, process.env.Admin_Token, {expiresIn: "20s"})
    return token
}
function generateEditorToken(User){
    const token = jwt.sign(User, process.env.Editor_Token,{expiresIn: "20s"})
    return token
}
function generateUserToken(User){
    const token = jwt.sign(User, process.env.User_Token,{expiresIn: "20s"})
    return token
}

module.exports = {
    generateAdminToken,
    generateEditorToken,
    generateUserToken
}