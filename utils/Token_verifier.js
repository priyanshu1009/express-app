const jwt = require("jsonwebtoken");
require('dotenv').config()
function verifyAdminToken(Admin) {
  const head = Admin.headers;
  const adminTokenValue = head.authorization;
  if (adminTokenValue) {
    const adminToken = adminTokenValue.split(" ")[1];

    try {
      const result = jwt.verify(adminToken, process.env.Admin_Token);
      return result;
    } catch (err) {
      return null;
    }
  } else {
    return null;
  }
}

function verifyEditorToken(Editor) {
  const head = Editor.headers;
  const editorTokenValue = head.authorization;
  if (editorTokenValue) {
    const editorToken = editorTokenValue.split(" ")[1];
    try {
      const result = jwt.verify(editorToken, process.env.Editor_Token);
      return result;
    } catch (err) {
      return null;
    }
  } else {
    return null;
  }
}

function verifyUserToken(User) {
  const head = User.headers;
  const userTokenValue = head.authorization;
  if (userTokenValue) {
    const userToken = userTokenValue.split(" ")[1];

    try {
      const result = jwt.verify(userToken, process.env.User_Token);
      return result;
    } catch (err) {
      return null;
    }
  } else {
    return null;
  }
}

module.exports = {
  verifyAdminToken,
  verifyEditorToken,
  verifyUserToken,
};
