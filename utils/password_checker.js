const bcrypt = require("bcrypt");

// The Password Checker Function
async function passChecker(password, hash) {
  const answer = await bcrypt.compare(password, hash);
  return answer;
}

module.exports = passChecker;
