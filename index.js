const express = require("express");
const app = express();
const admin = require("./routes/adminRoute");
const editor = require("./routes/editorRoute");
const user = require("./routes/userRoute");
const createAdmin = require("./routes/signup/adminSignUp");
const createEditor = require("./routes/signup/editorSignUp");
const createUser = require("./routes/signup/userSignUp");
const bodyParser = require("body-parser");
const connector = require("./db");
const adminLogger = require("./routes/login/adminLogin");
const editorLogger = require("./routes/login/editorLogin");
const userLogger = require("./routes/login/userLogin");
const swaggerUi = require("swagger-ui-express");
const swaggerDocuments = require("./utils/swaggerDocument.json")

const options = {
  explorer:true
}
connector();
app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocuments, options))
app.use(bodyParser.json());
app.use("/admin", admin);
app.use("/editor", editor);
app.use("/user", user);
app.use("/adminSignup", createAdmin);
app.use("/userSignup", createUser);
app.use("/editorSignup", createEditor);
app.use("/adminLogin", adminLogger);
app.use("/editorLogin", editorLogger);
app.use("/adminLogin", adminLogger);
app.use("/userLogin", userLogger);
app.listen(3000, () => {
  console.log("Listening at port 3000");
});
