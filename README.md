**Express App**

## This is a sample project made by me through express


*- In this I used express for api routing* 
*- BCrypt is used to encrypt and compare the psswords*
*- Mongoose is used to connect , perform query, save data from db (MongoDB)*
*- In this i used body-parse so that the request is accepted in other formats too*
*- To validate the body of request i used express-validator*
*- for the authentication i used JWT*
*- Swagger-express is used for the automated ui documentation of the api*

## To run this program first you have to clone it to your system and then run the "npm i " command in the terminal so that it installs all the modules needed and then just run the command node index.js after that the server will start listening at port 3000