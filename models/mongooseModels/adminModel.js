const mongoose = require('mongoose');
const { Schema } = mongoose
const adminSchema = new Schema({
    name :{
        type:String
    },
    password:{
        type:String,
        required:true
    },email:{
        type:String
    },createdAt: {
        default : Date.now(),
        type: Date   
    },lastLoggedIn:{
        type: Date
    },
})
const adminModel = mongoose.model('Admins',adminSchema)

module.exports = adminModel