const mongoose = require('mongoose');
const { Schema } = mongoose
const userSchema = new Schema({
    name :{
        type:String
    },
    password:{
        type:String,
        required:true
    },email:{
        type:String
    }
})
const userModel = mongoose.model('Users',userSchema)

module.exports = userModel