const mongoose = require('mongoose');
const { Schema } = mongoose
const editorSchema = new Schema({
    name :{
        type:String
    },
    password:{
        type:String,
        required:true
    },email:{
        type:String
    }
})
const editorModel = mongoose.model('Editors',editorSchema)

module.exports = editorModel