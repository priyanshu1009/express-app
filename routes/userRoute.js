const express = require('express');
const router = express.Router()
const jwtVerify = require('../utils/Token_verifier')


router.use((req,res,next)=>{
    const result = jwtVerify.verifyEditorToken(req)
    if (!result){
        res.sendStatus(403)
        return
    }
    next()
})

router.get('/read',(req,res)=>{
        res.send('User')
})

module.exports = router